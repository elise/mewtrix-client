use std::{
    collections::HashMap,
    str::FromStr,
    sync::mpsc::{channel, Receiver, Sender},
    thread::JoinHandle,
};

use chrono::{DateTime, Duration, Utc};
use linked_hash_map::LinkedHashMap;
use mewtrix::matrix::{
    api::{
        auth::{AuthenticationMode, UserIdentifier},
        events::{Presence, UnreadNotificationCount},
    },
    client::{MatrixClient, MatrixClientError},
    events::{
        room::{Message, NameContent, RedactionContent},
        DirectContent, Event, MatrixEvent,
    },
    identifier::{EventId, RoomId, UserId},
    room::MembershipState,
};
use sqlx::SqlitePool;
use url::Url;

pub trait Merge {
    fn merge(&mut self, rhs: Self);
}

impl Merge for RoomInfo {
    fn merge(&mut self, rhs: Self) {
        if rhs.name.is_some() {
            self.name = rhs.name;
        }

        if rhs.membership_state.is_some() {
            self.membership_state = rhs.membership_state;
        }

        if rhs.notifications.is_some() {
            self.notifications = rhs.notifications;
        }

        if let Some(new_messages) = rhs.messages {
            match &mut self.messages {
                Some(messages) => {
                    for (event_id, message_info) in new_messages {
                        if let Some(event) = messages.get_mut(&event_id) {
                            *event = message_info;
                        } else {
                            messages.insert(event_id, message_info);
                        }
                    }
                }
                None => self.messages = Some(new_messages),
            }
        }

        for (k, v) in rhs.redactions {
            self.redactions.insert(k, v);
        }
    }
}

#[derive(Debug, Clone)]
pub struct MessageInfo {
    pub event_id: EventId,
    pub content: Message,
    pub sender: UserId,
}

#[derive(Debug, Clone)]
pub struct RoomInfo {
    pub id: RoomId,
    pub name: Option<String>,
    pub membership_state: Option<MembershipState>,
    pub notifications: Option<UnreadNotificationCount>,
    pub messages: Option<LinkedHashMap<EventId, MessageInfo>>,
    pub redactions: HashMap<EventId, Option<String>>,
}

#[derive(Debug)]
pub enum ClientEvent {
    Direct(RoomId, UserId),
    RoomInfo(RoomInfo),
    UserPresence(UserId, Presence),
    Account(String),
    Error(MatrixLoopError),
}

pub enum EqEvent {
    Resync,
    CachedLogin(UserId),
    Login { user: UserId, password: String },
    JoinRoom(RoomId),
    LeaveRoom(RoomId),
    ForgetRoom(RoomId),
    SendMessage(RoomId, String),
    RedactEvent(RoomId, EventId),
}

struct InternalEqMpscHandle(Sender<ClientEvent>, Receiver<EqEvent>);

impl InternalEqMpscHandle {
    pub fn send_to_client(&self, event: ClientEvent) {
        if let Err(err) = self.0.send(event) {
            panic!("SendError<ClientEvent>: {:?}", err)
        }
    }

    pub fn eq_recv(&self) -> Option<EqEvent> {
        match self.1.try_recv() {
            Ok(res) => Some(res),
            Err(std::sync::mpsc::TryRecvError::Empty) => None,
            Err(std::sync::mpsc::TryRecvError::Disconnected) => {
                panic!("eqbound recv: TryRecvError::Disconnected")
            }
        }
    }

    pub async fn do_sync(&self, client: &mut MatrixClient) -> Result<(), MatrixClientError> {
        let sync_result = client.sync().await?;

        for event in sync_result.account_data.events {
            if let Event::Matrix(MatrixEvent::Direct(DirectContent { hashmap })) = event {
                for (user, rooms) in hashmap {
                    for room in rooms {
                        self.send_to_client(ClientEvent::Direct(room, user.clone()));
                    }
                }
            }
        }

        for (room_id, room) in sync_result.rooms.join {
            let room_name = room
                .state
                .events
                .iter()
                .rev()
                .find_map(|x| match &x.base.base {
                    mewtrix::matrix::events::Event::Matrix(MatrixEvent::Name(NameContent {
                        name,
                    })) => Some(name.clone()),
                    _ => None,
                });

            let mut messages = LinkedHashMap::new();
            let mut redactions = HashMap::new();

            for e in &room.timeline.events {
                if let Event::Matrix(MatrixEvent::Redaction(RedactionContent { reason })) = &e.base
                {
                    if let Some(id) = &e.redacts {
                        redactions.insert(id.clone(), reason.clone());
                    }
                }
            }

            for message in room.timeline.events.iter().filter_map(|e| {
                if let Event::Matrix(MatrixEvent::Message(x)) = &e.base {
                    Some(MessageInfo {
                        event_id: e.event_id.clone(),
                        content: x.clone(),
                        sender: e.sender.clone(),
                    })
                } else {
                    None
                }
            }) {
                messages.insert(message.event_id.clone(), message);
            }

            let messages = match messages.is_empty() {
                true => None,
                false => Some(messages),
            };

            self.send_to_client(ClientEvent::RoomInfo(RoomInfo {
                id: room_id.clone(),
                name: room_name,
                membership_state: Some(MembershipState::Join),
                notifications: Some(room.unread_notifications),
                messages,
                redactions,
            }));
        }

        for (room_id, room) in sync_result.rooms.invite {
            let room_name = room
                .invite_state
                .events
                .iter()
                .rev()
                .find_map(|x| match &x.event {
                    mewtrix::matrix::events::Event::Matrix(MatrixEvent::Name(NameContent {
                        name,
                    })) => Some(name.clone()),
                    _ => None,
                });

            self.send_to_client(ClientEvent::RoomInfo(RoomInfo {
                id: room_id,
                name: room_name,
                membership_state: Some(MembershipState::Invite),
                notifications: None,
                messages: None,
                redactions: HashMap::new(),
            }))
        }

        for (room_id, room) in sync_result.rooms.leave {
            let room_name = room
                .state
                .events
                .iter()
                .rev()
                .find_map(|x| match &x.base.base {
                    mewtrix::matrix::events::Event::Matrix(MatrixEvent::Name(NameContent {
                        name,
                    })) => Some(name.clone()),
                    _ => None,
                });

            self.send_to_client(ClientEvent::RoomInfo(RoomInfo {
                id: room_id,
                name: room_name,
                membership_state: Some(MembershipState::Leave),
                notifications: None,
                messages: None,
                redactions: HashMap::new(),
            }))
        }

        Ok(())
    }
}

#[derive(Debug)]
pub enum MatrixLoopError {
    MatrixClient(MatrixClientError),
    Url(url::ParseError),
}

impl From<url::ParseError> for MatrixLoopError {
    fn from(e: url::ParseError) -> Self {
        Self::Url(e)
    }
}

impl From<MatrixClientError> for MatrixLoopError {
    fn from(e: MatrixClientError) -> Self {
        Self::MatrixClient(e)
    }
}

pub struct MatrixEventQueue {
    clientbound_rx: Receiver<ClientEvent>,
    eqbound_tx: Sender<EqEvent>,
    thread_join: Option<JoinHandle<()>>,
}

impl MatrixEventQueue {
    async fn event_loop(
        sqlite_db_url: &str,
        handle: &InternalEqMpscHandle,
        matrix_client: &mut Option<MatrixClient>,
        prev_sync: &mut DateTime<Utc>,
    ) -> Result<(), MatrixLoopError> {
        let mut sync = false;
        while let Some(event) = handle.eq_recv() {
            match matrix_client {
                Some(matrix_client) => match event {
                    EqEvent::CachedLogin(_) => {}
                    EqEvent::Login { .. } => {}
                    EqEvent::JoinRoom(room_id) => {
                        matrix_client.join_room(room_id).await?;
                    }
                    EqEvent::Resync => {
                        matrix_client.since = None;
                        sync = true;
                    }
                    EqEvent::LeaveRoom(room_id) => {
                        matrix_client.leave_room(room_id).await?;
                    }
                    EqEvent::ForgetRoom(room_id) => {
                        matrix_client.forget_room(room_id).await?;
                    }
                    EqEvent::SendMessage(room_id, body) => {
                        matrix_client
                            .send_room_event(
                                room_id,
                                MatrixEvent::Message(mewtrix::matrix::events::room::Message {
                                    body,
                                    message: mewtrix::matrix::events::room::MessageType::Text {
                                        format: None,
                                        formatted_body: None,
                                    },
                                }),
                            )
                            .await?;
                    }
                    EqEvent::RedactEvent(room_id, event_id) => {
                        matrix_client.redact_event(room_id, event_id).await?;
                    }
                },
                None => match event {
                    EqEvent::CachedLogin(user) => {
                        let url = Url::parse(format!("https://{}", user.homeserver).as_str())?;

                        let mut client = MatrixClient::new(
                            SqlitePool::connect(sqlite_db_url)
                                .await
                                .expect("Failed to connect to DB"),
                            url,
                            user.clone(),
                        )
                        .await?;
                        client.try_cache_auth().await?;
                        let status = client.status(None).await?;

                        *matrix_client = Some(client);
                        handle.send_to_client(ClientEvent::UserPresence(user, status.presence));
                    }
                    EqEvent::Login { user, password } => {
                        let url = Url::parse(format!("https://{}", user.homeserver).as_str())?;

                        let mut client = MatrixClient::new(
                            SqlitePool::connect(sqlite_db_url)
                                .await
                                .expect("Failed to connect to DB"),
                            url,
                            user.clone(),
                        )
                        .await?;
                        client
                            .auth_request(
                                AuthenticationMode::Password {
                                    identifier: UserIdentifier::UserMatrixId {
                                        user: user.to_string(),
                                    },
                                    password,
                                },
                                None,
                            )
                            .await?;

                        let status = client.status(None).await?;

                        *matrix_client = Some(client);
                        handle.send_to_client(ClientEvent::UserPresence(user, status.presence));
                    }
                    _ => {}
                },
            }
        }

        if let Some(matrix_client) = matrix_client {
            if sync || Utc::now() - *prev_sync > Duration::seconds(2) {
                handle.do_sync(matrix_client).await?;
                *prev_sync = Utc::now();
            }
        }

        tokio::time::sleep(std::time::Duration::from_millis(250)).await;

        Ok(())
    }

    async fn event_queue(sqlite_db_url: String, handle: InternalEqMpscHandle) {
        let mut matrix_client = None;
        let mut prev_sync = Utc::now() - Duration::seconds(10);

        tokio::join!(async {
            loop {
                match Self::event_loop(
                    sqlite_db_url.as_str(),
                    &handle,
                    &mut matrix_client,
                    &mut prev_sync,
                )
                .await
                {
                    Ok(()) => {}
                    Err(why) => handle.send_to_client(ClientEvent::Error(why)),
                }
            }
        });
    }

    pub fn new(sqlite_db_url: String) -> Self {
        let (clientbound_tx, clientbound_rx) = channel();
        let (eqbound_tx, eqbound_rx) = channel();

        let thread_join = Some(std::thread::spawn(move || {
            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(async {
                    Self::event_queue(
                        sqlite_db_url,
                        InternalEqMpscHandle(clientbound_tx, eqbound_rx),
                    )
                    .await
                })
        }));

        Self {
            clientbound_rx,
            eqbound_tx,
            thread_join,
        }
    }

    pub fn get_all_valid_tokens(sqlite_db_url: String) -> Self {
        let (clientbound_tx, clientbound_rx) = channel();
        let (eqbound_tx, eqbound_rx) = channel();

        let thread_join = Some(std::thread::spawn(move || {
            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(async {
                    let handle = InternalEqMpscHandle(clientbound_tx, eqbound_rx);
                    let pool = SqlitePool::connect(sqlite_db_url.as_str())
                        .await
                        .expect("Failed to connect to DB");

                    for user in mewtrix::store::DeviceStore::all(&pool).await.unwrap() {
                        let user_id = UserId::from_str(user.matrix_id.as_str()).unwrap();
                        let homeserver =
                            Url::parse(format!("https://{}", user_id.homeserver).as_str()).unwrap();
                        if let Ok(mut client) = mewtrix::matrix::client::MatrixClient::new(
                            pool.clone(),
                            homeserver,
                            user_id,
                        )
                        .await
                        {
                            if let Ok(true) = client.try_cache_auth().await {
                                handle.send_to_client(ClientEvent::Account(user.matrix_id));
                            }
                        }
                    }
                })
        }));

        Self {
            clientbound_rx,
            eqbound_tx,
            thread_join,
        }
    }

    pub fn send_to_eq(&self, event: EqEvent) {
        if let Err(err) = self.eqbound_tx.send(event) {
            panic!("SendError<EqEvent>: {:?}", err)
        }
    }

    pub fn client_recv(&self) -> Option<ClientEvent> {
        match self.clientbound_rx.try_recv() {
            Ok(res) => Some(res),
            Err(_) => None,
        }
    }
}

impl Drop for MatrixEventQueue {
    fn drop(&mut self) {
        if let Some(jh) = self.thread_join.take() {
            jh.join().unwrap()
        }
    }
}
