use std::str::FromStr;

use eframe::epi::App;
use mewtrix::matrix::identifier::UserId;
use url::Url;

use crate::{client::ClientApp, event_queue::EqEvent, login::LoginApp};

pub enum MewtrixApp {
    Login(LoginApp),
    Client(ClientApp),
}

impl App for MewtrixApp {
    fn update(&mut self, ctx: &eframe::egui::CtxRef, frame: &mut eframe::epi::Frame<'_>) {
        match self {
            MewtrixApp::Login(e) => {
                e.update(ctx, frame);

                if e.clicked {
                    match e.enter_details {
                        true => {
                            if let Ok(user_id) = UserId::from_str(e.user.as_str()) {
                                match Url::parse(format!("https://{}", user_id.homeserver).as_str())
                                {
                                    Ok(url) => {
                                        match url.domain() == Some(user_id.homeserver.as_str()) {
                                            true => {
                                                let client_app = ClientApp::new(
                                                    e.sqlite_url.clone(),
                                                    user_id.clone(),
                                                    EqEvent::Login {
                                                        user: user_id,
                                                        password: e.password.clone(),
                                                    },
                                                );
                                                *self = MewtrixApp::Client(client_app);
                                            }
                                            false => e.error_hs_url = true,
                                        }
                                    }
                                    Err(_) => e.error_hs_url = true,
                                }
                            } else {
                                e.error_invalid_mxid = true;
                            }
                        }
                        false => match e.selected_user {
                            Some(idx) => {
                                let user_id = UserId::from_str(e.users[idx].as_str()).unwrap();
                                let client_app = ClientApp::new(
                                    e.sqlite_url.clone(),
                                    user_id.clone(),
                                    EqEvent::CachedLogin(user_id),
                                );
                                *self = MewtrixApp::Client(client_app);
                            }
                            None => unreachable!(),
                        },
                    }
                }
            }
            MewtrixApp::Client(client) => client.update(ctx, frame),
        }
    }

    fn name(&self) -> &str {
        match self {
            MewtrixApp::Login(e) => e.name(),
            MewtrixApp::Client(e) => e.name(),
        }
    }
}
