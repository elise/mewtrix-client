mod app;
mod client;
mod event_queue;
mod login;

use std::path::PathBuf;

use directories::ProjectDirs;
use login::LoginApp;
use mewtrix::matrix::{api::events::Presence, identifier::UserId};

use crate::app::MewtrixApp;

lazy_static::lazy_static! {
    pub static ref PROJECT_DIR: ProjectDirs = ProjectDirs::from("ltd", "headpats", "mewtrix").unwrap();
    pub static ref DB_PATH: PathBuf = {
        let mut path = PROJECT_DIR.data_dir().to_path_buf();
        std::fs::create_dir_all(&path).expect("Failed to create project directory");
        path.push("Mewtrix!.sqlite3");
        log::info!("DB Path: {:?}", path);
        std::fs::canonicalize(path).expect("Failed to canonicalize project directory")
    };
}

pub struct UserInfo {
    pub id: UserId,
    pub presence: Option<Presence>,
}

fn main() {
    env_logger::init();
    let app = MewtrixApp::Login(LoginApp::new(format!(
        "sqlite://{}",
        DB_PATH.to_str().unwrap()
    )));
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(Box::new(app), native_options);
}
