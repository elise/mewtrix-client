use std::{
    borrow::Cow,
    collections::{BTreeMap, HashMap},
};

use eframe::{
    egui::{self, Color32},
    epi,
};
use mewtrix::matrix::identifier::{RoomId, UserId};

use crate::{
    event_queue::{self, EqEvent, MatrixEventQueue, Merge, RoomInfo},
    UserInfo,
};

pub struct ClientApp {
    pub client: MatrixEventQueue,
    pub user_info: UserInfo,
    pub direct: HashMap<RoomId, UserId>,
    pub rooms: BTreeMap<RoomId, RoomInfo>,
    pub selected_room: Option<RoomId>,
    pub drafts: HashMap<RoomId, String>,
    pub leave: bool,
}

impl ClientApp {
    pub fn new(sqlite_db_url: String, user_id: UserId, login_event: EqEvent) -> Self {
        let client = MatrixEventQueue::new(sqlite_db_url);
        client.send_to_eq(login_event);
        client.send_to_eq(event_queue::EqEvent::Resync);
        Self {
            client,
            rooms: BTreeMap::new(),
            selected_room: None,
            user_info: UserInfo {
                id: user_id,
                presence: None,
            },
            direct: HashMap::new(),
            drafts: HashMap::new(),
            leave: false,
        }
    }

    fn room_name<'room_info>(&self, room_info: &'room_info RoomInfo) -> Cow<'room_info, str> {
        match &room_info.name {
            Some(name) => Cow::Borrowed(name),
            _ => match self.direct.get(&room_info.id) {
                Some(x) => Cow::Owned(x.to_string()),
                None => Cow::Owned(room_info.id.to_string()),
            },
        }
    }

    fn handle_loop(&mut self) {
        while let Some(event) = self.client.client_recv() {
            log::debug!("Recv Event: {:?}", event);
            match event {
                event_queue::ClientEvent::RoomInfo(room_info) => {
                    let room_info =
                        if let Some(mut old_room_info) = self.rooms.remove(&room_info.id) {
                            old_room_info.merge(room_info);
                            old_room_info
                        } else {
                            room_info
                        };
                    self.rooms.insert(room_info.id.clone(), room_info);
                }
                event_queue::ClientEvent::UserPresence(user_id, pres) => {
                    if self.user_info.id == user_id {
                        self.user_info.presence = Some(pres);
                    }
                }
                event_queue::ClientEvent::Direct(room, user) => {
                    self.direct.insert(room, user);
                }
                event_queue::ClientEvent::Account(_) => {} // Should not happen here
                event_queue::ClientEvent::Error(why) => panic!("{:?}", why),
            }
        }
    }

    fn forget_room(&mut self, room_id: &RoomId) {
        self.rooms.remove(room_id);
        self.direct.remove(room_id);
        if self.selected_room.as_ref() == Some(room_id) {
            self.selected_room = None;
        }

        self.client.send_to_eq(EqEvent::ForgetRoom(room_id.clone()));
    }

    fn leave_room(&mut self, room_id: &RoomId) {
        if let Some(mut x) = self.rooms.remove(room_id) {
            x.membership_state = Some(mewtrix::matrix::room::MembershipState::Leave);
        }

        self.client.send_to_eq(EqEvent::LeaveRoom(room_id.clone()));
    }
}

impl epi::App for ClientApp {
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        self.handle_loop();
        let text_style = egui::TextStyle::Heading;
        let heading_row_height = ctx.fonts()[text_style].row_height();

        egui::SidePanel::left("room_list").show(ctx, |ui| {
            match self.user_info.presence {
                Some(pres) => ui.heading(format!("{} ({})", self.user_info.id.to_string(), pres)),
                None => ui.heading(self.user_info.id.to_string()),
            };

            egui::ScrollArea::from_max_height(ui.available_height() - heading_row_height)
                .show_rows(ui, heading_row_height, self.rooms.len(), |ui, row_range| {
                    for (room_id, room_info) in self.rooms.iter().skip(row_range.start) {
                        let text = self.room_name(room_info);
                        ui.horizontal(|ui| {
                            let color = match room_info.membership_state {
                                Some(mewtrix::matrix::room::MembershipState::Join) => {
                                    Color32::from_rgb(0xFF, 0xAA, 0xBB)
                                }
                                Some(mewtrix::matrix::room::MembershipState::Invite) => {
                                    Color32::YELLOW
                                }
                                _ => Color32::RED,
                            };

                            if ui.add(egui::Button::new(text).text_color(color)).clicked() {
                                self.selected_room =
                                    match self.selected_room.as_ref() == Some(room_id) {
                                        true => None,
                                        false => Some(room_id.clone()),
                                    };

                                self.leave = false;
                                log::info!("SELECTED: {:?}", room_id);
                            }

                            if let Some(notifs) = &room_info.notifications {
                                let color = match notifs.highlight_count > 0 {
                                    true => Color32::RED,
                                    false => Color32::GREEN,
                                };

                                ui.add(
                                    egui::Label::new(notifs.notification_count.to_string())
                                        .text_color(color),
                                );
                            }
                        });
                    }
                });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| match self.selected_room.as_ref() {
                Some(room_id) => {
                    if let Some(room) = self.rooms.get(room_id).map(Clone::clone) {
                        let room_name = self.room_name(&room);
                        ui.horizontal(|ui| {
                            ui.heading(room_name.as_ref());
                            if !self.leave && ui.small_button("Leave").clicked() {
                                self.leave = true;
                            } else if self.leave && ui.small_button("Really Leave?").clicked() {
                                self.leave_room(&room.id);
                                self.leave = false;
                            }
                        });

                        if let Some(membership_state) = &room.membership_state {
                            match membership_state {
                                mewtrix::matrix::room::MembershipState::Invite => {
                                    ui.vertical_centered(|ui| {
                                        if ui
                                            .add(
                                                egui::Button::new("Join")
                                                    .text_color(Color32::BLACK)
                                                    .fill(Color32::GREEN),
                                            )
                                            .clicked()
                                        {
                                            self.client
                                                .send_to_eq(EqEvent::JoinRoom(room.id.clone()));
                                        } else if ui
                                            .add(
                                                egui::Button::new("Deny")
                                                    .text_color(Color32::BLACK)
                                                    .fill(Color32::RED),
                                            )
                                            .clicked()
                                        {
                                            self.leave_room(&room.id);
                                        }
                                    });
                                }
                                mewtrix::matrix::room::MembershipState::Join => {
                                    let num_rows =
                                        room.messages.as_ref().map(|x| x.len()).unwrap_or_default();
                                    egui::ScrollArea::from_max_height(
                                        ui.available_height() - heading_row_height * 3.0,
                                    )
                                    .show_rows(
                                        ui,
                                        heading_row_height,
                                        num_rows,
                                        |ui, range| {
                                            if let Some(msg) = &room.messages {
                                                for idx in range {
                                                    ui.horizontal_wrapped(|ui| {
                                                        let message =
                                                            msg.iter().nth(idx).unwrap().1;
                                                        ui.heading(message.sender.to_string());

                                                        if let Some(redaction) =
                                                            room.redactions.get(&message.event_id)
                                                        {
                                                            match redaction {
                                                                Some(reason) => ui.label(format!(
                                                                    "Redacted with reason: {}",
                                                                    reason
                                                                )),
                                                                None => ui.label("Redacted"),
                                                            };
                                                        } else {
                                                            ui.label(message.content.body.as_str());

                                                            if message.sender == self.user_info.id
                                                                && ui
                                                                    .add(
                                                                        egui::Button::new("DEL")
                                                                            .text_color(
                                                                                Color32::RED,
                                                                            ),
                                                                    )
                                                                    .clicked()
                                                            {
                                                                self.client.send_to_eq(
                                                                    EqEvent::RedactEvent(
                                                                        room.id.clone(),
                                                                        message.event_id.clone(),
                                                                    ),
                                                                );
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        },
                                    );
                                    ui.horizontal(|ui| {
                                        if !self.drafts.contains_key(&room.id) {
                                            self.drafts.insert(room.id.clone(), String::new());
                                        }

                                        if let Some(draft) = self.drafts.get_mut(&room.id) {
                                            ui.text_edit_multiline(draft);
                                            if ui
                                                .add(
                                                    egui::Button::new("Send")
                                                        .text_color(Color32::GREEN),
                                                )
                                                .clicked()
                                            {
                                                let mut msg = String::new();
                                                std::mem::swap(draft, &mut msg);
                                                self.client.send_to_eq(EqEvent::SendMessage(
                                                    room.id.clone(),
                                                    msg,
                                                ));
                                            }
                                        }
                                    });
                                }
                                mewtrix::matrix::room::MembershipState::Leave => {
                                    ui.vertical_centered(|ui| {
                                        if ui
                                            .add(
                                                egui::Button::new("Try Join")
                                                    .text_color(Color32::BLACK)
                                                    .fill(Color32::GREEN),
                                            )
                                            .clicked()
                                        {
                                            self.client
                                                .send_to_eq(EqEvent::JoinRoom(room.id.clone()));
                                        } else if ui
                                            .add(
                                                egui::Button::new("Forget")
                                                    .text_color(Color32::BLACK)
                                                    .fill(Color32::RED),
                                            )
                                            .clicked()
                                        {
                                            self.forget_room(&room.id);
                                        }
                                    });
                                }
                                mewtrix::matrix::room::MembershipState::Ban => {
                                    ui.centered_and_justified(|ui| {
                                        ui.add(
                                            egui::Label::new("Banned :c")
                                                .heading()
                                                .text_color(Color32::RED),
                                        );
                                    });
                                }
                                other => {
                                    ui.centered_and_justified(|ui| {
                                        ui.heading(format!("Unhandled Join State {:?}", other));
                                    });
                                }
                            }
                        }
                    }
                }
                None => {
                    ui.centered_and_justified(|ui| {
                        ui.add(
                            egui::Label::new("Mew!")
                                .heading()
                                .text_color(Color32::from_rgb(0xFF, 0xAA, 0xBB)),
                        );
                    });
                }
            });
        });
    }

    fn name(&self) -> &str {
        "Mewtrix! Client"
    }
}
