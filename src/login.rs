use eframe::{
    egui::{self, Color32},
    epi,
};
use mewtrix::matrix::client::{MatrixClientError, RequestClientError};

use crate::event_queue::{ClientEvent, MatrixEventQueue, MatrixLoopError};

pub struct LoginApp {
    pub sqlite_url: String,
    pub client: MatrixEventQueue,
    pub user: String,
    pub password: String,
    pub selected_user: Option<usize>,
    pub users: Vec<String>,
    pub enter_details: bool,
    pub clicked: bool,

    pub error_hs_url: bool,
    pub error_invalid_credentials: bool,
    pub error_network: bool,
    pub error_invalid_mxid: bool,
}

impl LoginApp {
    pub fn new(sqlite_url: String) -> Self {
        Self {
            client: MatrixEventQueue::get_all_valid_tokens(sqlite_url.clone()),
            user: String::new(),
            password: String::new(),
            sqlite_url,
            selected_user: None,
            users: Vec::new(),
            enter_details: true,
            clicked: false,
            error_hs_url: false,
            error_invalid_credentials: false,
            error_network: false,
            error_invalid_mxid: false,
        }
    }
}

impl epi::App for LoginApp {
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        while let Some(event) = self.client.client_recv() {
            match event {
                ClientEvent::Account(user) => self.users.push(user),
                ClientEvent::Error(err) => {
                    log::error!("ERROR: {:?}", err);
                    match err {
                        MatrixLoopError::Url(_) => self.error_hs_url = true,
                        MatrixLoopError::MatrixClient(MatrixClientError::ApiError(
                            RequestClientError::MatrixError(_, _),
                        )) => self.error_invalid_credentials = true,
                        MatrixLoopError::MatrixClient(MatrixClientError::ApiError(
                            RequestClientError::ReqwestError(_),
                        )) => self.error_network = true,
                        _ => {}
                    }
                }
                _ => {}
            }
        }

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.radio_value(&mut self.enter_details, true, "Enter Details");
                ui.radio_value(&mut self.enter_details, false, "Choose Account");
            });

            ui.vertical_centered(|ui| {
                match self.enter_details {
                    true => {
                        ui.heading("Username");
                        ui.text_edit_singleline(&mut self.user);
                        ui.heading("Password");
                        ui.text_edit_singleline(&mut self.password);

                        if self.error_hs_url {
                            ui.add(
                                egui::Label::new("Invalid Homeserver URL")
                                    .heading()
                                    .text_color(Color32::RED),
                            );
                        }
                        if self.error_invalid_credentials {
                            ui.add(
                                egui::Label::new("Invalid Credentials")
                                    .heading()
                                    .text_color(Color32::RED),
                            );
                        }
                        if self.error_network {
                            ui.add(
                                egui::Label::new("Network Error")
                                    .heading()
                                    .text_color(Color32::RED),
                            );
                        }
                        if self.error_invalid_mxid {
                            ui.add(
                                egui::Label::new("Invalid Matrix ID")
                                    .heading()
                                    .text_color(Color32::RED),
                            );
                        }
                    }
                    false => {
                        for user in 0..self.users.len() {
                            ui.radio_value(
                                &mut self.selected_user,
                                Some(user),
                                self.users[user].as_str(),
                            );
                        }
                    }
                }

                self.clicked = ui.small_button("Login").clicked()
                    && (self.enter_details | self.selected_user.is_some());
            });
        });
    }

    fn name(&self) -> &str {
        "Mewtrix! Login"
    }
}
